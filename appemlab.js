var express = require('express');
var userFile = require('./user.json');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');

var baseMlabURL = "https://api.mlab.com/api/1/databases/apiperu/collections/";
var apiKeyMlab = "apiKey=-Puknqeo7tEoHh5J9v4IgNoOLIiNhRuy"
var httpClient

var app = express();
var port = process.env.PORT||3000;
var URLbase = '/apiperu/v1/';
app.use(bodyParser.json());

//GET Users Mlab
app.get (URLbase + 'users',
  function(req, res){
    console.log('GET /apiperu/v1/users');
    httpClient = requestJSON.createClient(baseMlabURL);
    var queryString = 'f={"_id":0}&';
    httpClient.get('user?'+ queryString+apiKeyMlab,
      function(err, resM, body) {
        if (!err) {
          res.send(body)
          console.log("body", body);
        } else {
          console.log("resM", resM);
          res.send({"msg": "Error consulta"})
        }
      });
});

//GET Users ID
app.get (URLbase + 'users/:id',
  function(req, res){
    var id = req.params.id;
    console.log('GET /apiperu/v1/users/:id');
    httpClient = requestJSON.createClient(baseMlabURL);
    var queryString = 'q={"userID":' + id +'}&f={"_id":0}&';
    httpClient.get('user?'+ queryString+apiKeyMlab,
      function(err, resM, body) {
        console.log("err> ",err);
        console.log("resM.statusCode> ",resM.statusCode);
        console.log("body> ", body);
        if (resM.statusCode == 200) {
            if (body != null && body != '') {
              res.status(200).send(body[0]);
            } else {
              res.status(404).send({"msg": "Usuario no existe"});
            }
        } else {
          console.log("resM.statusCode> ",resM.statusCode);
          res.status(500).send({"msg": "Error consulta"})
        }
      });
});

//POST Users
app.post (URLbase + 'users',
  function(req, res){
    console.log('POST /apiperu/v1/users');
    httpClient = requestJSON.createClient(baseMlabURL);

    httpClient.get('user?'+apiKeyMlab,
      function(err, resM, body) {
         let usuarios =body;
         let tamanio = usuarios.length;
         let usr= usuarios[tamanio-1];
         let idmax = usr.userID;
         let newID = idmax + 1;
          console.log("newIDD", newID)
          var newUser = {
            "userID": newID,
            "first_name": req.body.first_name,
            "last_name": req.body.last_name,
            "email": req.body.email,
            "password": req.body.password
          }
          httpClient.post('user?'+apiKeyMlab,newUser,
            function(errPost, resPost, bodyPost) {
              console.log("bodyPost: ",  bodyPost);
              if (!errPost) {
                res.status(201).send({"msg": "Usuario añadido correctamente", bodyPost});
              } else {
                res.send({"msg": "Error resgistar Usuario"})
              }
            });
      });
});


app.put (URLbase + 'users/:id',
  function(req, res){
    console.log('PUT /apiperu/v1/users/:id');
    httpClient = requestJSON.createClient(baseMlabURL);
    var id = req.params.id;
    var queryString = 'q={"userID":' + id +'}&';
    httpClient.get('user?' + queryString + apiKeyMlab,
      function(err, resM, body) {
          var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
          httpClient.put(baseMlabURL + 'user?q={"userID": ' + id + '}&' + apiKeyMlab, JSON.parse(cambio),
           function(errorPut, respuestaMLabPut, bodyPut) {
             console.log("bodyPut:", bodyPut);
            res.status(201).send({"msg": "Usuario actualizado correctamente", bodyPut});
           });

      });
});

//DELETE user with id
app.delete(URLbase + "users/:id",
  function(req, res){
    console.log("entra al DELETE");
    console.log("request.params.id: " , req.params.id);
    var id = req.params.id;
    var queryStringID = 'q={"userID":' + id + '}&';
    console.log(baseMlabURL + 'user?' + queryStringID + apiKeyMlab);
    var httpClient = requestJSON.createClient(baseMlabURL);
    httpClient.get('user?' +  queryStringID + apiKeyMlab,
      function(error, respuestaMLab, body){
        var respuesta = body[0];
        console.log("body delete:"+ respuesta);
        httpClient.delete(baseMlabURL + "user/" + respuesta._id.$oid +'?'+ apiKeyMlab,
          function(error, respuestaMLab,body){
            res.send(body);
        });
      });
  });



  //LOGIN
  app.post (URLbase + 'loggin',
    function(req, res){
      console.log('POST /apiperu/v1/loggin');
      httpClient = requestJSON.createClient(baseMlabURL);
      var email = req.body.email
      var password = req.body.password
      console.log("user: ", email);
      console.log("pass: ", password);
      var queryString = 'q={"email":"' + email + '","password":"' + password + '"}'
      httpClient.get('user?' +  queryString + "&l=1&" + apiKeyMlab,
        function(errorGet, resGet, bodyGet){
          console.log("errorGet", errorGet);
          console.log("bodyGet", bodyGet);
          if (!errorGet) {
            if (bodyGet.length >= 1) {
              httpClient = requestJSON.createClient(baseMlabURL);
              var cambio = '{"$set":{"logged":true}}'
              httpClient.put('user?q={"userID": ' + bodyGet[0].userID + '}&' + apiKeyMlab, JSON.parse(cambio),
               function(errorPut, respuestaMLabPut, bodyPut) {
                 console.log("bodyPut:", bodyPut);
                res.status(201).send({"msg": "Loggin correctamente", bodyPut});
               });
            } else {
              res.status(404).send({"msg":'Usuario no encontrado'})
            }

          }
        });
  });

  //LOGOUT
  app.post (URLbase + 'logout',
    function(req, res){
      console.log('POST /apiperu/v1/logout');
      httpClient = requestJSON.createClient(baseMlabURL);
      var id = req.headers.id
      console.log("id: ", id);

      var queryString = 'q={"userID":' + id + ',"logged":true}&'

      httpClient.get('user?' +  queryString + apiKeyMlab,
        function(errorGet, resGet, bodyGet){
          console.log("errorGet", errorGet);
          console.log("bodyGet", bodyGet);
          if (!errorGet) {
            if (bodyGet.length >= 1) {
              httpClient = requestJSON.createClient(baseMlabURL);

              var cambio = '{"$set":{"logged":false}}'
              httpClient.put('user?q={"userID": ' + bodyGet[0].userID + '}&' + apiKeyMlab, JSON.parse(cambio),
               function(errorPut, respuestaMLabPut, bodyPut) {
                 console.log("bodyPut:", bodyPut);
                res.status(201).send({"msg": "logout correcto", bodyPut});
               });
            } else {
              res.status(404).send({"msg":'Usuario no logeado'})
            }

          }
        });
  });


app.listen(port);
