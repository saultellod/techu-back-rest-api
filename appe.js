var express = require('express');
var userFile = require('./user.json');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT||3001;
var URLbase = '/apiperu/v1/';
app.use(bodyParser.json());

//GET Users
/*
app.get (URLbase + 'users',
  function(req, res){
    console.log('GET /apiperu/v1/users');
    res.status(200).send(userFile);
});
*/


//GET Users ID
app.get (URLbase + 'users/:id',
  function(req, res){
    console.log('GET /apiperu/v1/users/:id');
    console.log("id: ", req.params.id);
    let pos = req.params.id
    let user = userFile[pos-1];
    if (user != null) {
      res.status(200).send(userFile[pos-1]);
    } else {
      res.status(404).send({"msg": "Usuario no existe"});
    }

});


//GET Users con query string
app.get (URLbase + 'users',
  function(req, res){
    console.log('GET /apiperu/v1/users');
    console.log("query: ", req.query);
    let pos = req.query.id
    let user = userFile[pos-1];
    if (user != null) {
      res.status(200).send(userFile[pos-1]);
    } else {
      res.status(404).send({"msg": "Usuario no existe"});
    }

});

//POST Users
app.post (URLbase + 'users',
  function(req, res){
    console.log('POST /apiperu/v1/users');
    let  newID = userFile.length + 1;
    var newUser = {
      "userID":newID,
      "first_name": req.body.first_name,
      "last_name": req.body.last_name,
      "email": req.body.email,
      "password": req.body.password
    }
    userFile.push(newUser);
    console.log(newUser);
    res.status(201).send({"msg": "Usuario añadido correctamente", newUser});
});

//PUT users
app.put (URLbase + 'users/:id',
  function(req, res){
    console.log('PUT /apiperu/v1/users/:id');
    let pos = req.params.id;
    let updateUser = userFile[pos-1];
    if (updateUser != null) {
      updateUser.first_name = req.body.first_name;
      updateUser.last_name = req.body.last_name,
      updateUser.email= req.body.email,
      updateUser.password = req.body.password
      console.log(updateUser);
      console.log(userFile);
      userFile.splice(pos-1,1,updateUser);
      res.status(200).send({"msg": "Usuario modificado correctamente"});
    } else {
      res.status(404).send({"msg": "Usuario no existe"});
    }

});


//DELETE users
app.delete (URLbase + 'users/:id',
  function(req, res){
    console.log('DELETE /apiperu/v1/users/:id');
    let pos = req.params.id;
    let deleteUser = userFile[pos-1];
    if (deleteUser != null) {
      console.log(pos);
      userFile.splice(pos-1,1);
      console.log(userFile);
      res.status(200).send({"msg": "Usuario eliminado correctamente"});
    } else {
      res.status(404).send({"msg": "Usuario no existe"});
    }
});

app.get('/', function (req, res) {
  res.send('Hola TechU!');
});

app.listen(port);
